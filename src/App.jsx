import "./App.css";
import { Box, Grommet, ResponsiveContext } from "grommet";
import LeftSide from "./components/LeftSide";
import RightSide from "./components/RightSide";
import { StyleSheetManager } from "styled-components";
import { grommet } from "grommet/themes";
import { useState } from "react";
const customTheme = {
  button: {
    hover: {
      boxShadow: "none",
    },
  },
};
function App() {
  const [isActive, setIsActive] = useState({
    num: 0,
  });
  return (
    <Box flex justify="center" align="center" className="App" height={"100vh"}>
      {/* <StyleSheetManager shouldForwardProp={(prop) => !prop.startsWith('level')}> */}
      <Grommet
        theme={{ ...grommet, ...customTheme }}
        style={{ borderRadius: "20px" }}
      >
        {/* <ResponsiveContext.Consumer> */}

        <Box
          round="medium"
          background={"white"}
          flex
          direction="row-responsive"
          margin={'20px'}
        >
          <LeftSide isActive={isActive} />
          <RightSide isActive={isActive} setIsActive={setIsActive} />
        </Box>
        {/* </ResponsiveContext.Consumer> */}
      </Grommet>
      {/* </StyleSheetManager> */}
    </Box>
  );
}

export default App;
