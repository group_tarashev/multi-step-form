import { Box, CheckBox, CheckBoxGroup, Text } from "grommet";
import React, { useState } from "react";
import OuterBox from "./OuterBox";

const addons = [
  {
    title: "Online service",
    desc: "Access to multiplayer games",
    priceMonth: 1,
    priceYear: 10,
    name: "online",
  },
  {
    title: "Larger storage",
    desc: "Extra 1TB of cloud save",
    priceMonth: 2,
    priceYear: 20,
    name: "storage",
  },
  {
    title: "Customizable profile",
    desc: "Custom theme on your profile",
    priceMonth: 2,
    priceYear: 20,
    name: "custom",
  },
];

const AddOns = ({ isMonth, setInputs, inputs, checkedName, setCheckedName }) => {
  
  const handleChage = (e, i) => {
    const { name, value, checked } = e.target;
    const input = { [name]: value };
    const chName = { [name]: i };
    const initialCheck = { [name]: null };
    const initialInput = { [name]: 0 };
    console.log(i);
    if (checked) {
      setCheckedName((prev) => ({ ...prev, ...chName }));
      setInputs((prev) => ({ ...prev, ...input }));
    } else {
      setInputs((prev) => ({ ...prev, ...initialInput }));
      setCheckedName((prev) => ({ ...prev, ...initialCheck }));
    }
  };

  const content = (
    <Box direction="column" gap="20px" style={{ marginTop: "40px" }}>
      {addons.map((item, i) => (
        <Box
          key={i}
          width={"100%"}
          border="all"
          pad={"20px"}
          round={"small"}
          direction="row"
          justify="between"
          align="center"
          background={
            (inputs.online || inputs.storage || inputs.custom) !== 0 &&
            (checkedName.online === i ||
              checkedName.storage === i ||
              checkedName.custom === i) &&
            "#c7daed"
          }
        >
          <Box direction="row" gap="20px">
            <CheckBox
            checked={(inputs.online || inputs.storage || inputs.custom) !== 0 &&
                (checkedName.online === i ||
                  checkedName.storage === i ||
                  checkedName.custom === i) && true}
              name={item.name}
              value={isMonth ? item.priceMonth : item.priceYear}
              onChange={(e) => handleChage(e, i)}
            />
            <Box>
              <Text>{item.title}</Text>
              <Text color={"grey"} size="16px">
                {item.desc}
              </Text>
            </Box>
          </Box>
          <Text>
            +${isMonth ? item.priceMonth + "/mo" : item.priceYear + "/yr"}
          </Text>
        </Box>
      ))}
    </Box>
  );
  return (
    <Box>
      <OuterBox
        head={"Pick add-ons"}
        para={"Add-ons help enchance your gaming experience "}
        children={content}
      />
    </Box>
  );
};

export default AddOns;
