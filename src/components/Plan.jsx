import { Box, Image, Text } from "grommet";
import React, { useState } from "react";
import OuterBox from "./OuterBox";
import '../style/plan.css'
const cards = [
  {
    icon: "../src/assets/images/icon-arcade.svg",
    title: "Arcade",
    priceMonth: 9,
    priceYear: 90,
  },
  {
    icon: "../src/assets/images/icon-advanced.svg",
    title: "Advanced",
    priceMonth: 12,
    priceYear: 120,
  },
  {
    icon: "../src/assets/images/icon-pro.svg",
    title: "Pro",
    priceMonth: 15,
    priceYear: 150,
  },
];

const Plan = ({select, setSelect, isMonth, setIsMonth, setInputs}) => {
  const handleClick = (i) =>{
    setSelect(prev => ({...prev, num: i}))
    if(isMonth){
        setInputs(prev =>({...prev, plan: cards[i].title, planPrice: cards[i].priceMonth}))
    }else{
        setInputs(prev =>({...prev, plan: cards[i].title, planPrice: cards[i].priceYear}))

    }
  }
  const handleMonth = () =>{
    setIsMonth(!isMonth)
    setSelect(prev => ({...prev, num: null}))
    
  }
  const content = (
    <Box width={'100%'}>
      <Box className="cards" direction="row" gap="20px" style={{marginTop:40}}>
        {cards.map((item, i) => {
          return (
            <Box
            key={i}
            border="all"
              width={"200px"}
              height={"200px"}
              justify="between"
              round="small"
              pad={"small"}
              background={ select.num === i && '#c7daed'}
              onClick={() => handleClick(i)}
              className="card"
            >
              <Image src={item.icon} width={45} height={45} />
              <Box>
                <Text color={'#092542'}>{item.title}</Text>
                <Text color={'#abafb3'}>
                  ${isMonth ? item.priceMonth + "/mo" : item.priceYear + "/yr"}
                </Text>
                <Text color={'#092542'}>{isMonth || "2 month free"}</Text>
              </Box>
            </Box>
          );
        })}
      </Box>
      <Box
        direction="row"
        gap="20px"
        justify="center"
        width={"100%"}
        style={{
          marginTop: "50px",
          backgroundColor: "#c7daed",
          padding: 20,
        }}
      >
        <Text color={isMonth && "#092542"}>Monthly</Text>
        <div onClick={() => handleMonth()} className="switch-btn">
          <span className={!isMonth ? "toggler active" : "toggler"}></span>
        </div>
        <Text color={isMonth || "#092542"}>Yearly</Text>
      </Box>
    </Box>
  );
  return (
    <Box flex align="center" justify="center" pad={0}>
      <OuterBox
        head={"Select your plan"}
        para={"You have the option of monthly or yearly billing."}
        children={content}
      />
    </Box>
  );
};

export default Plan;
