import { Box, Button, Grommet } from "grommet";
import React, { useEffect, useState } from "react";
import Info from "./Info";
import styled from "styled-components";
import Plan from "./Plan";
import AddOns from "./AddOns";
import Summary from "./Summary";
import Thank from "./Thank";

const ButtonStyled = styled(Button)`
  color: white;
  border: none;
  background-color: #092542;
  font-weight: bolder;
  height: 50px;
`;

const RightSide = ({ isActive, setIsActive }) => {
  const [chechInputs, setCheckInputs] = useState(false);
  const [isMonth, setIsMonth] = useState(true);
  const [select, setSelect] = useState({
    num: 0,
  });
  const [inputs, setInputs] = useState({
    name: "",
    email: "",
    phone: "",
    mothly: true,
    online: 0,
    storage: 0,
    custom: 0,
    plan: "Arcade",
    planPrice: 9,
    totalPrice: 0,

  });
  console.log(inputs);
  const [checkedName, setCheckedName] = useState({
    online: null,
    storage: null,
    custom: null,
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    const input = { [name]: value };
    setInputs((prev) => ({ ...prev, ...input }));
  };
  useEffect(() => {
    if (
      inputs.email.length !== 0 &&
      inputs.name.length !== 0 &&
      inputs.phone.length !== 0 &&
      inputs.email.includes("@")
    ) {
      setCheckInputs(true);
    } else {
      setCheckInputs(false);
    }
  }, [inputs]);
  return (
    <Box pad={"xsmall"} width={{max: '600px'}}  flex={'grow'}  justify="center" align="center">
      <Box flex className="right" justify="center" align="center">
        {isActive.num === 0 && (
          <Info handleChange={handleChange} inputs={inputs} />
        )}
        {isActive.num === 1 && (
          <Plan
            select={select}
            setSelect={setSelect}
            isMonth={isMonth}
            setIsMonth={setIsMonth}
            setInputs={setInputs}
          />
        )}
        {isActive.num === 2 && (
            <AddOns isMonth={isMonth} setInputs={setInputs} inputs={inputs} checkedName={checkedName} setCheckedName={setCheckedName}/>
        )}
        {isActive.num === 3 && (
            <Summary inputs={inputs} isMonth={isMonth} setIsActive={setIsActive}/>
        )}
        {isActive.num === -1 && (
            <Thank/>
        )}
      </Box>
      <Box  direction="row" justify={isActive.num > 0 ? 'between' : "end"} align="center" width={{max:"550px"}}>
        {isActive.num > 0 && (
          <ButtonStyled
            onClick={() => {
              chechInputs &&
                setIsActive((prev) => ({ ...prev, num: prev.num - 1 }));
            }}
            disabled={!chechInputs}
            alignSelf="start"
            label=" Go Back"
            style={{
                backgroundColor:'transparent',
                color: 'gray'
            }}
          />
        )}
        {isActive.num >= 0 && isActive.num < 3 && <ButtonStyled
          onClick={() => {
            chechInputs &&
              setIsActive((prev) => ({ ...prev, num: prev.num + 1 }));
          }}
          disabled={!chechInputs || select.num === null}
          alignSelf="end"
          justify="end"
          label="Next Step"
        />}
        {isActive.num === 3 && 
            <ButtonStyled
                label="Confirm"
                alignSelf="end"
                onClick={() => setIsActive(prev => ({...prev, num: -1}))}
            />
        }
      </Box>
    </Box>
  );
};

export default RightSide;
