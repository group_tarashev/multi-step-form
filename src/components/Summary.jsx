import { Box, Text } from "grommet";
import React from "react";
import OuterBox from "./OuterBox";
import '../style/summary.css'
const Summary = ({ inputs, isMonth, setIsActive }) => {
  const adds = (service, price) => {
    return (
      <Box direction="row" justify="between">
        <Text color={"grey"}>{service}</Text>
        <Text>
          +${price}
          {isMonth ? "/mo" : "/yr"}
        </Text>
      </Box>
    );
  };
  const content = (
    <Box>
      <Box className="summary" margin={"40px"} background={"#c7daed"} pad={"40px"} round="small">
        <Box direction="row" justify="between" align="center">
          <Box>
            <Text margin={"5px"}>
              {inputs.plan} ({isMonth ? "Monthly" : "Yearly"})
            </Text>
            <Text
              margin={"5px"}
              style={{ textDecoration: "underline", cursor:'pointer' }}
              onClick={() => setIsActive((prev) => ({ ...prev, num: 1 }))}
              
            >
              change
            </Text>
          </Box>
          <Text>
            ${isMonth ? inputs.planPrice + "/mo" : inputs.planPrice + "/yr"}
          </Text>
        </Box>
        <hr style={{ marginTop: 20, marginBottom: 20 }} />
        <Box margin={10}>
          {inputs.online !== 0 && adds("Online service", inputs.online)}
          {inputs.storage !== 0 && adds("Larger storage", inputs.storage)}
          {inputs.custom !== 0 && adds("Custumizalbe profile", inputs.custom)}
        </Box>
      </Box>
      <Box margin={'10px'} direction="row" justify="between" width={"80%"} alignSelf="center">
        <Text>Total (per {isMonth ? "month" : "year"})</Text>
        <Text>
          +$
          {parseInt(inputs.planPrice) +
            parseInt(inputs.online) +
            parseInt(inputs.storage) +
            parseInt(inputs.custom)}
          {isMonth ? "/mo" : "/yr"}
        </Text>
      </Box>
    </Box>
  );
  return (
    <Box>
      <OuterBox
        head={"Finishing up"}
        para={"Double-check everything-looks OK before confiming"}
        children={content}
      />
    </Box>
  );
};

export default Summary;
