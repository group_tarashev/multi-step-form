import {
  Box,
  Button,
  Form,
  FormField,
  Heading,
  Paragraph,
  Text,
  TextInput,
} from "grommet";
import React from "react";
import OuterBox from "./OuterBox";

const Info = ({handleChange, inputs}) => {
  const input = (name, label, placeholder, value, type) => {
    return (
      <Box flex style={{margin:"10px 0"}}>
        <label
          style={{
            color: "#092542",
            fontWeight: "bolder",
            fontSize: "18px",
            marginBottom: "10px",
          }}
          htmlFor={name}
          >
          {label}
        </label>
        <TextInput
        type={type}
            value={value}
          id={name}
          name={name}
          focusIndicator={false}
          style={{
            border: "1px solid grey",
            height: 40,
            borderRadius: "10px",
            fontSize: 18,
          }}
          onChange={handleChange}
          required={true}
          placeholder={placeholder}
          minLength={6}
        />
      </Box>
    );
  };
  const form = (
    <Form style={{ margin: "20px 0" }}>
        {input("name", "Name", "e.g John Doe", inputs.name, 'text')}
        {input("email", "Email", "e.g johndoe@mail.com", inputs.email, 'email')}
        {input('phone', 'Phone Number', 'e.g. +1 234 567 890', inputs.phone, 'text')}
      </Form>
  )
  return (
    <OuterBox
        head={'Personal Info'}
        para={'Pleace provide your name, email address, and phone number'}
        children={form}
    />
      
    
  );
};

export default Info;
