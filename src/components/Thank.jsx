import { Box, Image, Text } from 'grommet'
import React from 'react'

const Thank = () => {
  return (
    <Box direction='column' gap='30px' align='center' margin={'50px'}>
        <Image src='../src/assets/images/icon-thank-you.svg'/>
        <Text size='40px'>Thank you!</Text>
        <Text size='16px' color={'grey'} textAlign='center'>Thanks for confiming your subscribtion! We hope you have fun using our platform. If you ever need support, please feel free to email us at iliyan.tarashev@gmail.com</Text>
    </Box>
  )
}

export default Thank