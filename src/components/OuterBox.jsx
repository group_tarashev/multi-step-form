import { Box, Heading, Paragraph } from 'grommet'
import React from 'react'
import '../style/outer.css'
const OuterBox = ({head, para, children}) => {
  return (
    <Box className='outer' align='center' justify='center' style={{width:'80%', height:'100%', margin:'20px'}}>
      <Heading style={{marginBottom: '10px'}} className='outer-head' size='30px' margin={"none"} color={'#092542'}>{head}</Heading>
      <Paragraph size='18px' style={{width: '100%', color:'#8a939c', fontSize:18, marginBottom: '10px'}}>
        {para}
      </Paragraph>
      {children}
    </Box>
  )
}

export default OuterBox