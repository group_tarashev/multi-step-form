import { Sidebar, Image, Tabs, Tab, Box, Text, RadioButton } from "grommet";
import React, { useEffect, useLayoutEffect, useState } from "react";
import "../style/sidebar.css";
const LeftSide = ({ isActive }) => {
  const tabs = ["Your info", "select plan", "add-ons", "summary"];
  const [color, setColor] = useState(false);
  const style = (i) => ({
    width: 40,
    height: 40,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "100%",
    backgroundColor: isActive.num !== i && "transparent",
    backgroundColor: isActive.num === i && "skyblue",
    color: "white",
    border: "1px solid white",
    border: isActive.num !== i && "1px solid white",
    padding: 10,
  });
  useEffect(() => {
    // Use the useEffect to set color after the component has re-rendered
    setColor(true); // or any condition you want to check
  }, [isActive]);
  const tabsAppend = tabs.map((item, i) => {
    return (
      <Box key={i} direction="row-responsive" alignContent="center" gap="20px">
        <Text style={style(i)}>{i + 1}</Text>
        <Box className="side-option">
          <Text color={"#c1ccd9"}>STEP {i + 1}</Text>
          <Text color={"white"} style={{ fontWeight: "bolder" }}>
            {item.toUpperCase()}
          </Text>
        </Box>
      </Box>
    );
  });

  return (
    <Sidebar
      background={{
          image: "url(../src/assets/images/bg-sidebar-desktop.svg)",
        }}
        flex={'shrink'}
        width={'100%'}
        className="sidebar-image"
        // round={{size:'15px'}}
    >
      <Box className="sidebar" style={{ zIndex: 1, padding: 40 }} gap="40px">
        {tabsAppend}
      </Box>
    </Sidebar>
  );
};

export default LeftSide;
